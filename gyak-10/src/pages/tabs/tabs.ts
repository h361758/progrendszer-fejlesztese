import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { WebPage } from '../web/web';
import { TwoWayPage } from '../two-way/two-way';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;
  tab4Root = WebPage;
  tab5Root = TwoWayPage;

  constructor() {

  }
}
