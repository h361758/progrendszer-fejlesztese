import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-two-way',
  templateUrl: 'two-way.html'
})
export class TwoWayPage {

  text: string;

  constructor(public navCtrl: NavController) {

  }

}
