import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { RestService } from '../../service/rest.client';

@Component({
  selector: 'page-web',
  templateUrl: 'web.html'
})
export class WebPage {

  message: string;

  constructor(public navCtrl: NavController, private _rest : RestService) {

  }

  getMessage() {
      this._rest.getJson().subscribe(
          result => this.message = result,
          error => console.error(error)
      );
  }

}
