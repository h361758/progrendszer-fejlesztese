import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class RestService {
    constructor(private _http: Http) {

    }

    getJson(): Observable<any> {
        return this._http.get('http://jsonplaceholder.typicode.com/posts/1')
            .map(result => result.json());
    }
}