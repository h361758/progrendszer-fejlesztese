var Numbers = (function () {
    function Numbers(a, b) {
        this.a = a;
        this.b = b;
    }
    Numbers.prototype.add = function () {
        return this.a + this.b;
    };
    Numbers.prototype.multiply = function () {
        return this.a * this.b;
    };
    return Numbers;
}());
var nums;
function ok() {
    var a = Number(document.getElementById('a').value);
    var b = Number(document.getElementById('b').value);
    nums = new Numbers(a, b);
}
function add() {
    document.getElementById('result').innerHTML = String(nums.add());
}
function multiply() {
    document.getElementById('result').innerHTML = String(nums.multiply());
}
