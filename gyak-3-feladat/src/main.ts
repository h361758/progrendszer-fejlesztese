class Numbers {
  a: number;
  b: number;

  constructor(a: number, b: number) {
    this.a = a;
    this.b = b;
  }

  add() {
    return this.a + this.b;
  }

  multiply() {
    return this.a * this.b;
  }
}

var nums: Numbers;

function ok() {
  var a = Number((<HTMLInputElement>document.getElementById('a')).value)
  var b = Number((<HTMLInputElement>document.getElementById('b')).value)

  nums = new Numbers(a, b);
}

function add() {
  document.getElementById('result').innerHTML = String(nums.add());
}

function multiply() {
  document.getElementById('result').innerHTML = String(nums.multiply());
}
