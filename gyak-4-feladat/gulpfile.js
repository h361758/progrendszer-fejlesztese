var gulp = require('gulp')
    webpack = require('webpack-stream');

gulp.task('build', function() {
    return gulp.src('./src/main.ts')
        .pipe(webpack(require('./webpack.config.js')))
        .pipe(gulp.dest('dist'));
});
