import { Pet } from './pet';

export class Human {
    name: string;
    age: number;

    pet: Pet;

    constructor(name: string, age: number) {
        this.name = name;
        this.age = age;
    }

    createPet(name: string) {
        this.pet = new Pet(name);
    }

    feed() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (this.pet) {
                    resolve(this.pet.eat('cat food'));
                } else {
                    reject('No pet!');
                }
            }, 1000);
        });
    }

    greet() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (this.age > 18) {
                    resolve('Ok');
                } else {
                    reject('Underage!');
                }
            }, 1000);
        });
    }
}
