import { Human } from './human';

var human: Human;

module.exports = {

    create: function() {
        var name = (<HTMLInputElement>document.getElementById('nameField')).value;
        var age = Number((<HTMLInputElement>document.getElementById('ageField')).value);

        human = new Human(name, age);
    },

    createPet: function() {
        if (human) {
            var petName = (<HTMLInputElement>document.getElementById('petNameField')).value;

            human.createPet(petName);
        }
    },

    feed: function() {
        human.feed().then((text: string) => {
            document.getElementById('petMessageArea').innerHTML = text;
        }).catch((error: string) => {
            document.getElementById('petMessageArea').innerHTML = error;
        });
    },

    greet: function() {
        human.greet().then((text: string) => {
            document.getElementById('messageArea').innerHTML = text;
        }).catch((error: string) => {
            document.getElementById('messageArea').innerHTML = error;
        });
    }

};
