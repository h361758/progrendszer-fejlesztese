export class Pet {
    name: string;

    constructor(name: string) {
        this.name = name;
    }

    eat(food: string) {
        return this.name + ' ate ' + food;
    }
}