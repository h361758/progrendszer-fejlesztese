var Entry =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	const human_1 = __webpack_require__(1);
	var human;
	module.exports = {
	    create: function () {
	        var name = document.getElementById('nameField').value;
	        var age = Number(document.getElementById('ageField').value);
	        human = new human_1.Human(name, age);
	    },
	    greet: function () {
	        human.greet().then((text) => {
	            document.getElementById('messageArea').innerHTML = text;
	        }).catch((error) => {
	            document.getElementById('messageArea').innerHTML = error;
	        });
	    }
	};


/***/ },
/* 1 */
/***/ function(module, exports) {

	"use strict";
	class Human {
	    constructor(name, age) {
	        this.name = name;
	        this.age = age;
	    }
	    greet() {
	        return new Promise((resolve, reject) => {
	            setTimeout(() => {
	                if (this.age > 18) {
	                    resolve('Ok');
	                }
	                else {
	                    reject('Underage!');
	                }
	            }, 1000);
	        });
	    }
	}
	exports.Human = Human;


/***/ }
/******/ ]);