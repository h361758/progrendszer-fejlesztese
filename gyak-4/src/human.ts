export class Human {
    name: string;
    age: number;

    constructor(name: string, age: number) {
        this.name = name;
        this.age = age;
    }

    greet() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (this.age > 18) {
                    resolve('Ok');
                } else {
                    reject('Underage!');
                }
            }, 1000);
        });
    }
}
