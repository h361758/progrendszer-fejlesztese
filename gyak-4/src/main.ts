import { Human } from './human';

var human: Human;

module.exports = {

    create: function() {
        var name = (<HTMLInputElement>document.getElementById('nameField')).value;
        var age = Number((<HTMLInputElement>document.getElementById('ageField')).value);

        human = new Human(name, age);
    },

    greet: function() {
        human.greet().then((text: string) => {
            document.getElementById('messageArea').innerHTML = text;
        }).catch((error: string) => {
            document.getElementById('messageArea').innerHTML = error;
        });
    }

};
