module.exports = {
    entry: './src/main.ts',
    output: {
        libraryTarget: 'var',
        library: 'Entry',
        filename: 'bundle.js'
    },
    resolve: {
        extensions: [ '', '.webpack.js', 'web.js', '.ts', '.js' ]
    },
    module: {
        loaders: [
            { test: /\.tsx?$/, loader: 'ts-loader' }
        ]
    }
};
