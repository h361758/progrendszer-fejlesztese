var express = require('express');

var router = express.Router();

router.route('/media').get((request, response) => {
  response.status(200).send();
}).post((request, response) => {
  response.status(404).send();
});

router.route('/user').put((request, response) => {
  console.log('This has been called.');
  response.status(500).send();
})

module.exports = router;