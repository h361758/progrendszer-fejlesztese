var express = require('express'),
    router = express.Router();

router.route('/').get((request, response) => {
  response.status(200).send('Hello.');
})

var app = express();

app.use('/', router);

app.use('/hello', require('./routes/routes'));
app.use('/world', require('./routes/routes'));

app.listen(3000, () => {
  console.log('Server is up and running...');
});