var express = require('express');
var router = express.Router();
router.route('/').get(function (request, response) {
    // console.log(request);
    response.status(200).send('Hello.');
}).post(function (request, response) {
    response.status(404).send('Not found.');
}).delete(function (request, response) {
    response.status(200).send('Done.');
});
router.route('/').put(function (request, response) {
    response.status(200).send('Put done.');
});
router.route('/address').post(function (request, response) {
    response.status(500).send('You failed!');
});
module.exports = router;
