var express = require('express');
// import * as express from 'express'
var app = express();
app.use('/', require('./routes/routes'));
app.use('/test', require('./routes/routes'));
app.listen(5000, function () {
    console.log('Server is up and running...');
});
