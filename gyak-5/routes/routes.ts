var express = require('express');

var router = express.Router();

router.route('/').get((request, response) => {
  // console.log(request);
  response.status(200).send('Hello.');
}).post((request, response) => {
  response.status(404).send('Not found.');
}).delete((request, response) => {
  response.status(200).send('Done.');
});

router.route('/').put((request, response) => {
  response.status(200).send('Put done.');
})

router.route('/address').post((request, response) => {
  response.status(500).send('You failed!');
});

module.exports = router;