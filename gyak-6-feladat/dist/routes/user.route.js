var express = require('express');
var router = express.Router();
module.exports = function (passport) {
    router.route('/greeting').get(function (req, res, next) {
        if (req.isAuthenticated()) {
            return res.status(200).end('OK');
        }
        else {
            return res.status(403).end('FORBIDDEN');
        }
    }).post(function (req, res, next) {
        passport.authenticate('login', function (error, user) {
            if (error) {
                res.status(403).send('Forbidden!');
            }
            else {
                req.login(user, function (error, user) {
                    if (error) {
                        res.status(500).send('Error during serialization.');
                    }
                    else {
                        res.status(200).send('Welcome!');
                    }
                });
            }
        })(req, res, next);
    });
    return router;
};
