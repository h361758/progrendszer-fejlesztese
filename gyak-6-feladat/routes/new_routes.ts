var express = require('express');

var router = express.Router();

module.exports = (passport) => {

  router.route('/login').post((req, res, next) => {
    passport.authenticate('login', (error, user) => {
      if (error) {
        res.status(400).send('Incorrect login details!');
      } else {
        req.login(user, (error, user) => {
          if (error) {
            res.status(500).send('Error during serialization.');
          } else {
            res.status(200).send('Welcome!');
          }
        });
      }
    })(req, res, next);
  });

  router.route('/logout').post((req, res, next) => {
    req.logout();
    res.status(200).send('Logged out');
  });


  router.route('/hello').get((req, res, next) => {
    if (req.isAuthenticated()) {
      return res.status(200).end('Hello');
    } else {
      return res.status(401).end('Unauthorized');
    }
  });

  return router;
};