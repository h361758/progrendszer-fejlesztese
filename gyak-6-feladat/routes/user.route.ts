var express = require('express');

var router = express.Router();

module.exports = (passport) => {
  router.route('/greeting').get((req, res, next) => {
    if (req.isAuthenticated()) {
      return res.status(200).end('OK');
    } else {
      return res.status(403).end('FORBIDDEN');
    }
  }).post((req, res, next) => {
    passport.authenticate('login', (error, user) => {
      if (error) {
        res.status(403).send('Forbidden!');
      } else {
        req.login(user, (error, user) => {
          if (error) {
            res.status(500).send('Error during serialization.');
          } else {
            res.status(200).send('Welcome!');
          }
        });
      }
    })(req, res, next);
  });

  return router;
};