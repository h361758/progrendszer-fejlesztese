var express = require('express'),
    expressSession = require('express-session'),
    passport = require('passport'),
    LocalStrategy = require('passport-local'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser');

var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});

passport.use('login', new LocalStrategy.Strategy((username, password, done) => {
  if (username === 'test' && password === 'test') {
    done(null, username);
  } else {
    done('ERROR', username);
  }
}));

app.use(expressSession({ secret: 'nodejspassportdemosecretkeyword' }));
app.use(passport.initialize());
app.use(passport.session());

app.use('/rest/user', require('./routes/user.route')(passport));

app.listen(5000, () => {
  console.log('The server is running');
});
