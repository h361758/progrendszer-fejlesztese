"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
// import * as bcrypt from 'bcrypt';
var bcrypt = require('bcrypt');
exports.AppleSchema = new mongoose.Schema({
    type: String,
    weight: Number,
    color: String
});
exports.Apple = mongoose.model('Apple', exports.AppleSchema);
