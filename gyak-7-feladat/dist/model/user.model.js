"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
// import * as bcrypt from 'bcrypt';
var bcrypt = require('bcrypt');
var SALT_WORK_FACTOR = 10;
exports.UserSchema = new mongoose.Schema({
    username: String,
    password: String
});
exports.UserSchema.pre('save', function preSaveCallback(next) {
    var self = this;
    if (!self.isModified('password')) {
        next();
    }
    else {
        bcrypt.genSalt(SALT_WORK_FACTOR, function genCallback(error, salt) {
            if (error) {
                return next(error);
            }
            else {
                bcrypt.hash(self.password, salt, function hashCallback(error, hash) {
                    if (error) {
                        next(error);
                    }
                    else {
                        self.password = hash;
                        next();
                    }
                });
            }
        });
    }
});
exports.UserSchema.methods.comparePassword = function comparePassword(password, callback) {
    bcrypt.compare(password, this.password, function compareCallback(error, isMatch) {
        if (error) {
            return callback(error);
        }
        else {
            callback(null, isMatch);
        }
    });
};
exports.User = mongoose.model('User', exports.UserSchema);
