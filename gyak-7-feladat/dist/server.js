"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var expressSession = require("express-session");
var passport = require("passport");
var LocalStrategy = require("passport-local");
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var mongoose = require("mongoose");
var user_model_1 = require("./model/user.model");
var app = express();
var router = express.Router();
var dbUrl = 'mongodb://test:asdasd@localhost:27017/prf';
app.set('dbUrl', dbUrl);
mongoose.connect(dbUrl);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
passport.serializeUser(function (user, done) {
    done(null, user);
});
passport.deserializeUser(function (user, done) {
    done(null, user);
});
passport.use('login', new LocalStrategy.Strategy(function (username, password, done) {
    user_model_1.User.findOne({ username: username }, function (error, temp_user) {
        if (error)
            return done(error);
        if (!temp_user)
            return done(null, false);
        temp_user.comparePassword(password, function (error, isMatch) {
            if (error)
                done(error);
            return done(null, temp_user);
        });
    });
}));
app.use(expressSession({ secret: 'nodejspassportdemosecretkeyword' }));
app.use(passport.initialize());
app.use(passport.session());
app.use('/rest/user', require('./routes/user.route')(passport, router));
app.listen(5000, function () {
    console.log('The server is running');
});
