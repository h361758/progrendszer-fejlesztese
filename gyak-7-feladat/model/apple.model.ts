import * as mongoose from 'mongoose';
// import * as bcrypt from 'bcrypt';

var bcrypt = require('bcrypt');

export interface IApple extends mongoose.DocumentInterface {
  type: string;
  weight: number;
  color: string;
}

export const AppleSchema = new mongoose.Schema({
  type: String,
  weight: Number,
  color: String
});

export const Apple: mongoose.model<IApple> = mongoose.model<IApple>('Apple', AppleSchema);