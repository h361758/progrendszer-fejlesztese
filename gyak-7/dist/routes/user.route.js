"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var user_model_1 = require("../model/user.model");
module.exports = function (passport, router) {
    router.post('/register', function (req, res, next) {
        if (!req.body.username || !req.body.password) {
            res.status(400).send('Registration data needed!');
        }
        else {
            var user = new user_model_1.User({
                username: req.body.username,
                password: req.body.password
            });
            user.save();
            res.status(200).send('Registration successful!');
        }
    });
    router.route('/login').post(function (req, res, next) {
        passport.authenticate('login', function (error, user) {
            if (error) {
                res.status(403).send('Forbidden!');
            }
            else {
                req.login(user, function (error, user) {
                    if (error) {
                        res.status(500).send('Error during serialization.');
                    }
                    else {
                        res.status(200).send('Welcome!');
                    }
                });
            }
        })(req, res, next);
    });
    return router;
};
