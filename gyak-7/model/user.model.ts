import * as mongoose from 'mongoose';
// import * as bcrypt from 'bcrypt';

var bcrypt = require('bcrypt');

var SALT_WORK_FACTOR = 10;

export interface IUser extends mongoose.DocumentInterface {
  username: string;
  password: string;
}

export const UserSchema = new mongoose.Schema({
  username: String,
  password: String
});

UserSchema.pre('save', function preSaveCallback(next) {
  var self = this;

  if (!self.isModified('password')) {
    next();
  } else {
    bcrypt.genSalt(SALT_WORK_FACTOR, function genCallback(error, salt) {
      if (error) {
        return next(error);
      } else {
        bcrypt.hash(self.password, salt, function hashCallback(error, hash) {
          if (error) {
            next(error);
          } else {
            self.password = hash;
            next();
          }
        });
      }
    });
  }
});

UserSchema.methods.comparePassword = function comparePassword(password, callback) {
  bcrypt.compare(password, this.password, function compareCallback(error, isMatch) {
    if (error) {
      return callback(error);
    } else {
      callback(null, isMatch);
    }
  });
};

export const User: mongoose.model<IUser> = mongoose.model<IUser>('User', UserSchema);