import { User } from '../model/user.model';

module.exports = (passport, router) => {

  router.post('/register', (req, res, next) => {
    if (!req.body.username || !req.body.password) {
      res.status(400).send('Registration data needed!');
    } else {
      var user = new User({
        username: req.body.username,
        password: req.body.password
      });

      user.save();
      res.status(200).send('Registration successful!');
    }
  });

  router.route('/login').post((req, res, next) => {
    passport.authenticate('login', (error, user) => {
      if (error) {
        res.status(403).send('Forbidden!');
      } else {
        req.login(user, (error, user) => {
          if (error) {
            res.status(500).send('Error during serialization.');
          } else {
            res.status(200).send('Welcome!');
          }
        });
      }
    })(req, res, next);
  });

  return router;
};