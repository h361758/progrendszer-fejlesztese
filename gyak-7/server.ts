import * as express from 'express';
import * as expressSession from 'express-session';
import * as passport from 'passport';
import * as LocalStrategy from 'passport-local';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';

import * as mongoose from 'mongoose';

import { User } from './model/user.model';

var app = express();
var router = express.Router();

var dbUrl = 'mongodb://test:asdasd@localhost:27017/prf';
app.set('dbUrl', dbUrl);
mongoose.connect(dbUrl);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});

passport.use('login', new LocalStrategy.Strategy((username, password, done) => {
  User.findOne({ username: username }, (error, temp_user) => {
    if (error) return done(error);
    if (!temp_user) return done(null, false);

    temp_user.comparePassword(password, (error, isMatch) => {
      if (error) done(error);
      return done(null, temp_user);
    });
  })
}));

app.use(expressSession({ secret: 'nodejspassportdemosecretkeyword' }));
app.use(passport.initialize());
app.use(passport.session());

app.use('/rest/user', require('./routes/user.route')(passport, router));

app.listen(5000, () => {
  console.log('The server is running');
});
