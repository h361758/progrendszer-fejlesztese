import { Component } from '@angular/core';

@Component({
  selector: "multiplier",
  templateUrl: "./app.component3.html"
})
export class AppComponent3 {

  num: number = 1;

  multiply() {
    this.num = this.num * 2;
  }

}