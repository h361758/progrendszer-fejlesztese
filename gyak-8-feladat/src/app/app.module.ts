import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { AppComponent, AppComponent2 }  from './app.component';
import { AppComponent3 }  from './app.component3';

@NgModule({
  imports:      [ BrowserModule, FormsModule ],
  declarations: [ AppComponent, AppComponent2, AppComponent3 ],
  bootstrap:    [ AppComponent, AppComponent2, AppComponent3 ]
})
export class AppModule { }
