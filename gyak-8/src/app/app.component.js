"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var AppComponent = (function () {
    function AppComponent() {
        this.name = 'Angular';
        this.users = [];
        this.users.push('George', 'Philip', 'John');
        this.removed = false;
    }
    AppComponent.prototype.save = function (user) {
        this.users.push(user);
    };
    AppComponent.prototype.remove = function (user) {
        var index = this.users.indexOf(user);
        var deleted = this.users.splice(index, 1);
        if (deleted) {
            this.removed = true;
        }
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'my-app',
        templateUrl: "./app.component.html",
    }),
    __metadata("design:paramtypes", [])
], AppComponent);
exports.AppComponent = AppComponent;
var AppComponent2 = (function () {
    function AppComponent2() {
        this.new_name = 'component2';
    }
    AppComponent2.prototype.ngOnInit = function () {
        this.hello();
    };
    AppComponent2.prototype.hello = function () {
        console.log('Hello ' + this.new_name);
    };
    return AppComponent2;
}());
AppComponent2 = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'my-cmp',
        templateUrl: 'app.component2.html'
    }),
    __metadata("design:paramtypes", [])
], AppComponent2);
exports.AppComponent2 = AppComponent2;
//# sourceMappingURL=app.component.js.map