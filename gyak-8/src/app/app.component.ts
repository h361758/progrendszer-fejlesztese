import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'my-app',
  templateUrl: `./app.component.html`,
})
export class AppComponent  {
  
  name = 'Angular';
  
  users: string[] = [];
  removed: boolean;

  constructor() {
    this.users.push('George', 'Philip', 'John');
    this.removed = false;
  }

  save(user: string) {
    this.users.push(user);
  }

  remove(user: string) {
    let index = this.users.indexOf(user);
    let deleted = this.users.splice(index, 1);
    if (deleted) {
      this.removed = true;
    }
  }

}

@Component({
  moduleId: module.id,
  selector: 'my-cmp',
  templateUrl: 'app.component2.html'
})
export class AppComponent2 implements OnInit {

  new_name: string;

  constructor() {
    this.new_name = 'component2';
  }

  ngOnInit() {
    this.hello();
  }

  hello() {
    console.log('Hello ' + this.new_name);
  }

}