import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent, AppComponent2 }  from './app.component';

@NgModule({
  imports:      [ BrowserModule ],
  declarations: [ AppComponent, AppComponent2 ],
  bootstrap:    [ AppComponent, AppComponent2 ]
})
export class AppModule { }
