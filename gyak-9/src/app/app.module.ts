import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { WebModule } from './web/web.module';
import { GreetingModule } from './greeting/greeting.module';

import { AppComponent } from './app.component';
import { appRoutes } from './app.routes';

@NgModule({
  imports:      [ BrowserModule, HttpModule, FormsModule, WebModule, GreetingModule, 
                  RouterModule.forRoot(appRoutes)],
  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ]
})

export class AppModule { }
