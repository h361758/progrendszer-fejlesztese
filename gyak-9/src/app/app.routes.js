"use strict";
var web_route_1 = require('./web/web.route');
var greeting_route_1 = require('./greeting/greeting.route');
var web_component_1 = require('./web/web.component');
exports.appRoutes = web_route_1.WebRoutes.concat(greeting_route_1.GreetingRoutes, [
    { path: '**', component: web_component_1.WebComponent }
]);
//# sourceMappingURL=app.routes.js.map