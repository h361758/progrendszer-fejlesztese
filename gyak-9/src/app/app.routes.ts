import { RouterModule, Routes } from '@angular/router';

import { WebRoutes } from './web/web.route';
import { GreetingRoutes } from './greeting/greeting.route';

import { WebComponent } from './web/web.component';


export const appRoutes: Routes = [
  ...WebRoutes,
  ...GreetingRoutes,
  { path: '**', component: WebComponent }
];
