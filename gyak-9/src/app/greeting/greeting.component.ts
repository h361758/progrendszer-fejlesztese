import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'my-greeting',
  templateUrl: 'greeting.component.html',
})
export class GreetingComponent {

    name: string;

    constructor(private route: ActivatedRoute) {
        this.route.params.subscribe(params => {
            this.name = params['user'];
        });
    }

}
