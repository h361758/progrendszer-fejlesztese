import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { GreetingComponent } from './greeting.component';

@NgModule({
  imports:      [ BrowserModule ],
  declarations: [ GreetingComponent ]
})

export class GreetingModule { }
