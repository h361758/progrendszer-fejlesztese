"use strict";
var greeting_component_1 = require('./greeting.component');
exports.GreetingRoutes = [
    { path: 'greeting/:user', component: greeting_component_1.GreetingComponent }
];
//# sourceMappingURL=greeting.route.js.map