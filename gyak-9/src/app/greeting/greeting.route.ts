import { Routes } from '@angular/router';

import { GreetingComponent } from './greeting.component';

export const GreetingRoutes: Routes = [
  { path: 'greeting/:user', component: GreetingComponent }
];
