"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var web_service_1 = require('./web.service');
var router_1 = require('@angular/router');
var WebComponent = (function () {
    function WebComponent(router, _webService) {
        this.router = router;
        this._webService = _webService;
        this.users = [];
        this.users.push('George', 'Philip', 'John');
    }
    WebComponent.prototype.ngOnInit = function () {
        this.onGetPosts();
    };
    WebComponent.prototype.save = function (user) {
        this.users.push(user);
        this.name = '';
    };
    WebComponent.prototype.remove = function (user) {
        var index = this.users.indexOf(user);
        var deleted = this.users.splice(index, 1);
    };
    WebComponent.prototype.onGetPosts = function () {
        var _this = this;
        this._webService.getData().subscribe(function (response) {
            _this.resp = response;
        }, function (err) {
            console.error(err);
        });
    };
    WebComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-web',
            templateUrl: 'web.component.html',
        }), 
        __metadata('design:paramtypes', [router_1.Router, web_service_1.WebService])
    ], WebComponent);
    return WebComponent;
}());
exports.WebComponent = WebComponent;
//# sourceMappingURL=web.component.js.map