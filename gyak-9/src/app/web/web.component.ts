import { Component } from '@angular/core';
import { WebService } from './web.service';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'my-web',
  templateUrl: 'web.component.html',
})
export class WebComponent implements OnInit  {
  users: string[] = [];
  name: string;
  resp: any;

  constructor(private router: Router, private _webService: WebService) {
    this.users.push('George', 'Philip', 'John');
  }

  ngOnInit() {
      this.onGetPosts();
  }

  save(user: string) {
    this.users.push(user);
    this.name = '';
  }

  remove(user: string) {
    let index = this.users.indexOf(user);
    let deleted = this.users.splice(index, 1);
  }

  onGetPosts() {
    this._webService.getData().subscribe(
      response => {
        this.resp = response;
      },
      err => {
        console.error(err);
      }
    );
  }

}
