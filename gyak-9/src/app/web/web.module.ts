import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { WebComponent } from './web.component';
import { WebService } from './web.service';

@NgModule({
  imports:      [ BrowserModule, HttpModule, FormsModule],
  declarations: [ WebComponent ],
  providers:    [ WebService ],
  bootstrap:    [ WebComponent ]
})

export class WebModule { }
