import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WebComponent } from './web.component';

export const WebRoutes: Routes = [
  { path: 'web', component: WebComponent }
];
