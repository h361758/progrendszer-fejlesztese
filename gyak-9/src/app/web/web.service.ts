import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class WebService {

    constructor(private http: Http) { }

    getData(): Observable<any> {
        return this.http.get('http://jsonplaceholder.typicode.com/posts/1').map(res => res.json());
    }

}
